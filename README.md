# mudc

![](https://img.shields.io/badge/written%20in-C%2B%2B%2C%20Javascript-blue)

An interactive javascript environment based on TinyJS.

Includes libraries for HTTP, NMDC, Twitter, SABnzbd+, Cleverbot, and uTorrent support.

Contains an ad-hoc implementation of half of curses/readline for a console user interface.

Included samples:
- Banner text for NMDC
- NMDC/Twitter integration
- Chat bot for NMDC based on Cleverbot
- uTorrent and SABnzbd+ bots for NMDC

Source code and API documentation included in download.

As TinyJS is made available under the LGPL, the (trivial) modifications to build it as a standalone library were previously available https://code.google.com/p/tinyjs-dll/ but have since been removed. The source code bundle includes all modifications and additional (standard ECMA) functions added to the interpreter.

This eventually superceded a number of previous standalone projects related to NMDC.

Tags: nmdc


## Download

- [⬇️ mudc-1.4.7z](dist-archive/mudc-1.4.7z) *(120.46 KiB)*
- [⬇️ mudc-1.3.7z](dist-archive/mudc-1.3.7z) *(94.75 KiB)*
- [⬇️ mudc-1.2.rar](dist-archive/mudc-1.2.rar) *(98.43 KiB)*
- [⬇️ mudc-1.1.rar](dist-archive/mudc-1.1.rar) *(94.16 KiB)*
- [⬇️ mudc-1.0.rar](dist-archive/mudc-1.0.rar) *(132.38 KiB)*
